<?php

namespace ChemicalsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Molecule
 *
 * @ORM\Table(name="molecule")
 * @ORM\Entity(repositoryClass="ChemicalsBundle\Repository\MoleculeRepository")
 */
class Molecule
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="formula", type="string", length=255, nullable=true, unique=true)
     */
    private $formula;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Molecule
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set formula
     *
     * @param string $formula
     *
     * @return Molecule
     */
    public function setFormula($formula)
    {
        $this->formula = $formula;

        return $this;
    }

    /**
     * Get formula
     *
     * @return string
     */
    public function getFormula()
    {
        return $this->formula;
    }
    
    public function __toString() {
        return $this->name . ' (' . $this->formula . ')';
    }
}

