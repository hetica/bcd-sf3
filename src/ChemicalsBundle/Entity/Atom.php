<?php
// src/ChemicalsBundle/Entity/Atom.php

namespace ChemicalsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Atom
 *
 * @ORM\Table(name="atom")
 * @ORM\Entity(repositoryClass="ChemicalsBundle\Repository\AtomRepository")
 */
class Atom
{
    /**
     * 
     * 
     * @ORM\ManyToOne(targetEntity="ChemicalsBundle\Entity\Element")
     * @ORM\JoinColumn(nullable=true)
     */
    private $element ;
    
    public function setElement(Element $element = null)
    {
        $this->element = $element ;
        return $this ;
    }
    
    public function getElement()
    {
        return $this->element ;
    }

    /**
     * 
     * @ORM\ManyToOne(targetEntity="ChemicalsBundle\Entity\Molecule")
     * @ORM\JoinColumn(nullable=false) 
     *
     */
    private $molecule ;

    public function setMolecule(Molecule $molecule)
    {
        $this->molecule = $molecule ;
        return $this ;
    }
    
    public function getMolecule()
    {
        return $this->molecule ;
    }
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Atom
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    public function __toString() {
        return $this->name ;
    }
}

