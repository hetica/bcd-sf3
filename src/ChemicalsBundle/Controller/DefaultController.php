<?php

namespace ChemicalsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ChemicalsBundle\Entity\Element;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager() ;
        $elementNb = $em->getRepository("ChemicalsBundle:Element")->getElementNb();
        $atomNb = $em->getRepository("ChemicalsBundle:Atom")->getAtomNb() ;
        $moleculeNb = $em->getRepository("ChemicalsBundle:Molecule")->getMoleculeNb() ;
        
        //return $this->render('ChemicalsBundle:Default:index.html.twig');
        return $this->render('chemicals/index.html.twig',
                [
                    'elementNb' => $elementNb,
                    'atomNb' => $atomNb,
                    'moleculeNb' => $moleculeNb,
                ]);
    }
}
