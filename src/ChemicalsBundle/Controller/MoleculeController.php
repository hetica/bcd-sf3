<?php
namespace ChemicalsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ChemicalsBundle\Entity\Molecule;

/**
 * Molecule controller.
 *
 * @author Aurélien MULLER
 */
class MoleculeController extends Controller
{ 
    /**
     * Display molecules from periodic table.
     * 
     * @param type $page
     * @param type $maxPerPage
     *
     * @return type
     */
    public function listAction($page, $maxPerPage)
    {
        // Let's get the molecule repository.
        $r = $this->getDoctrine()->getRepository("ChemicalsBundle:Molecule");

        $molecules = $r->getListPaginated($page, $maxPerPage);
        $pagination = array(
                        'page' => $page,
                        'nbPages' => ceil(count($molecules) / $maxPerPage),
                        'max' => $maxPerPage,
                        'routeName' => 'chemicals_molecules_list',
                        'routeParams' => array()
        );

        return $this->render(
                        'chemicals/molecules.list.html.twig',
                        [
                                        'molecules' => $molecules,
                                        'pagination' => $pagination,
                        ]);
    }

    /**
     * Affiche le detail sur un des  molecules 
     */
    public function getAction($id)
    {
        $em = $this->getDoctrine()->getManager() ;
        
        // get molecule 
        $molecule = $em
                ->getRepository("ChemicalsBundle:Molecule")
                ->findBy(['id' => $id]) ;   	            
        
        $linkAtoms = $em
                ->getRepository("ChemicalsBundle:Atom")
                ->findBy(['molecule' => $molecule ]) ;

        // Root path of render is app/Resources/views.
        return $this->render(
                        'chemicals/molecules.detail.html.twig',
                        [
                                        'molecule' => $molecule,        // renvoie l'objet molecule
                                        'linkAtoms' => $linkAtoms
                        ]);
    }
}

