<?php
namespace ChemicalsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ChemicalsBundle\Entity\Element;

/**
 * Element controller.
 *
 * @author Aurélien MULLER
 */
class ElementController extends Controller
{ 
    /**
     * Display elements from periodic table.
     * 
     * @param type $page
     * @param type $maxPerPage
     *
     * @return type
     */
    public function listAction($page, $maxPerPage)
    {
        // Let's get the element repository.
        $r = $this->getDoctrine()->getRepository("ChemicalsBundle:Element");

        $elements = $r->getListPaginated($page, $maxPerPage);
        $pagination = array(
                        'page' => $page,
                        'nbPages' => ceil(count($elements) / $maxPerPage),
                        'max' => $maxPerPage,
                        'routeName' => 'chemicals_elements_list',
                        'routeParams' => array()
        );

        return $this->render(
                        'chemicals/elements.list.html.twig',
                        [
                                        'elements' => $elements,
                                        'pagination' => $pagination,
                        ]);
    }

    /**
     * Affiche le detail sur un des  elements 
     */
    public function getAction($id)
    {
        $r = $this->getDoctrine()->getRepository("ChemicalsBundle:Element");
        // $element = $r->findByID($id) ;                               // alternative
        $element = $r->findBy(['id' => $id]) ;   	               // permet de faire plus de choses

        // Root path of render is app/Resources/views.
        return $this->render(
                        'chemicals/element.detail.html.twig',
                        [
                                        'element' => $element,                  // renvoie l'objet element
                        ]);
    }

    /**
     * Asks a webservice for data about elements from Mendeleiev table.
     *
     * This data is then used to create entities: elements.
     */
    public function createSampleElementsFromWebserviceAction()
    {
        try {
            // Let's get the RESTHelper service.
            $ws = $this->get("chemicals.helper");
            $periodicElements = $ws->getPeriodicElements();

            if (!empty($periodicElements)) {
                // Doctrine Manager is used to 
                $em = $this->getDoctrine()->getManager();
                foreach ($periodicElements as $formula => $name) {
                    // Declaration of a new Element entity.
                    $element = new Element();
                    $element->setFormula($formula);
                    $element->setName($name);

                    // Index the entity within Doctrine.
                    // That method DOES NOT launch any request
                    // to MYSQL server.
                    $em->persist($element);
                }
                // Flush method sends all the stored requests to MySQL.
                $em->flush();
                // AddFlash is a method storing messages that are displayed once.
                // Check documentation to know how to display them within Twig template
                // (base.html.twig)
                $this->addFlash(
                    'notice',
                    'Atoms and periodic elements were created successfully.'
                );
            } else {
                $this->addFlash(
                    'warning',
                    'Nothing was acquired from webservice.'
                );
            }
        } catch (Exception $ex) {
            $this->addFlash(
                'error',
                'An error occurred while creating elements. Check the logs for more information.'
            );
            // Log that error.
            // $this->get('logger')->error($ex->getMessage());
        }

        // Once the process is done, redirect the user to the page listing
        // all the elements contained within the database.
        // RedirectToRoute has for parameter the key of a route,
        // found in Resources/config/route.yml
        return $this->redirectToRoute("chemicals_elements_list");
    }

}
