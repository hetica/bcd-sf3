<?php
namespace ChemicalsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ChemicalsBundle\Entity\Atom;

/**
 * Atom controller.
 *
 * @author Aurélien MULLER
 */
class AtomController extends Controller
{ 
    /**
     * Display atoms from periodic table.
     * 
     * @param type $page
     * @param type $maxPerPage
     *
     * @return type
     */
    public function listAction($page, $maxPerPage)
    {
        // Let's get the atom repository.
        $r = $this->getDoctrine()->getRepository("ChemicalsBundle:Atom");

        $atoms = $r->getListPaginated($page, $maxPerPage);
        $pagination = array(
                        'page' => $page,
                        'nbPages' => ceil(count($atoms) / $maxPerPage),
                        'max' => $maxPerPage,
                        'routeName' => 'chemicals_atoms_list',
                        'routeParams' => array()
        );

        return $this->render(
                        'chemicals/atoms.list.html.twig',
                        [
                                        'atoms' => $atoms,
                                        'pagination' => $pagination,
                        ]);
    }

    /**
     * Affiche le detail sur un des  atoms 
     */
    public function getAction($id)
    {
        $em = $this->getDoctrine()->getManager() ;
        $r = $em->getRepository("ChemicalsBundle:Atom");
        // $atom = $r->findByID($id) ;                               // alternative
        $atom = $r->findBy(['id' => $id]) ;   	               // permet de faire plus de choses
        
        // Root path of render is app/Resources/views.
        return $this->render(
                        'chemicals/atoms.detail.html.twig',
                        [
                                        'atom' => $atom,                  // renvoie l'objet atom
                        ]);
    }
}

