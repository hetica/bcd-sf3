<?php
// src/AppBundle/Controller/AtomController.php

namespace AppBundle\Controller ;

use Symfony\Bundle\FrameworkBundle\Controller\Controller ;
use ChemicalsBundle\Form\AtomType ;
use ChemicalsBundle\Entity\Atom ;
use Symfony\Component\HttpFoundation\Request ;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/*
 * Atom controller.
 * 
 * @author: Benoit Guibert
 */
class AtomController extends Controller 
{
    /*
     * add Atom.
     * 
     */
    public function addAction(Request $request)
    {
        $atom = new Atom() ;
        
        $form = $this->createForm(AtomType::class, $atom) ;
        $form->handleRequest($request) ;
        
        if ($form->isSubmitted() && $form->isValid()) {
            // process métier
            $em = $this->getDoctrine()->getManager() ;
            $em->persist($atom) ;
            $em->flush() ;
            $this->addFlash(
                'notice', 
                'Atom ' . $atom->getName() . ' created successfully.' 
            );
            return $this->redirectToRoute('chemicals_atoms_list');
        }
        
        return $this->render("chemicals/atoms.add.html.twig", [
            "form" => $form->createView() ,
        ]);
    }


    /*
     * deletes Atom.
     * 
     * @param type $id
     * 
     * @return type
     */
    public function deleteAction(Request $request, $id)
    {
        $rep = $this->getDoctrine()->getRepository('ChemicalsBundle:Atom');
        $atom = $rep->findBy(['id' => $id]) ; 
        if(!empty($atom)) {
            $atom = $atom[0] ;
        } else {
            $this->addFlash(
                    'warning' ,
                    'Atom ' . $id . ' not found.'
                    ) ;
        }
        $form = $this->createForm(AtomType::class, $atom) ;
        $form->handleRequest($request) ;        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager() ;
            $em->remove($atom) ;
            $em->flush() ;
            $this->addFlash(
                    'notice', 
                    'Atom ' . $id . ' deleted successfully.' 
                    ) ;
            return $this->redirectToRoute("chemicals_atoms_list") ;
            }       
        return $this->render("chemicals/atoms.delete.html.twig", [
            "form" => $form->createView() ,
        ]);
    }
    
    public function editAction(Request $request, $id)
    {
        $rep = $this->getDoctrine()->getRepository('ChemicalsBundle:Atom') ;
        $atom = $rep->findBy(['id' => $id]) ; 
        
        if (!empty($atom))
        {
            $atom = $atom[0] ;
        }
        
        $form = $this->createForm(AtomType::class, $atom) ;
        $form->handleRequest($request) ;
        
        if ($form->isSubmitted() && $form->isValid()) {
            // process métier
            $em = $this->getDoctrine()->getManager() ;
            $em->persist($atom) ;
            $em->flush() ;
            $this->addFlash(
                'notice', 
                'Atom ' . $atom->getName() . ' modified successfully.' 
            );
            return $this->redirectToRoute('chemicals_atoms_list');
        }
        return $this->render("chemicals/atoms.edit.html.twig", [
            "form" => $form->createView() ,
        ]);
    }
}

