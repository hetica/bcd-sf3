<?php
// src/AppBundle/Controller/MoleculeController.php

namespace AppBundle\Controller ;

use Symfony\Bundle\FrameworkBundle\Controller\Controller ;
use ChemicalsBundle\Form\MoleculeType ;
use ChemicalsBundle\Entity\Molecule ;
use Symfony\Component\HttpFoundation\Request ;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/*
 * Molecule controller.
 * 
 * @author: Benoit Guibert
 */
class MoleculeController extends Controller 
{
    /*
     * add Molecule.
     * 
     */
    public function addAction(Request $request)
    {
        $molecule = new Molecule() ;
        
        $form = $this->createForm(MoleculeType::class, $molecule) ;
        $form->handleRequest($request) ;
        
        if ($form->isSubmitted() && $form->isValid()) {
            // process métier
            $em = $this->getDoctrine()->getManager() ;
            $em->persist($molecule) ;
            $em->flush() ;
            $this->addFlash(
                'notice', 
                'Molecule ' . $molecule->getName() . ' created successfully.' 
            );
            return $this->redirectToRoute('chemicals_molecules_list');
        }
        
        return $this->render("chemicals/molecules.add.html.twig", [
            "form" => $form->createView() ,
        ]);
    }


    /*
     * deletes Molecule.
     * 
     * @param type $id
     * 
     * @return type
     */
    public function deleteAction(Request $request, $id)
    {
        $rep = $this->getDoctrine()->getRepository('ChemicalsBundle:Molecule');
        $molecule = $rep->findBy(['id' => $id]) ; 
        if(!empty($molecule)) {
            $molecule = $molecule[0] ;
        } else {
            $this->addFlash(
                    'warning' ,
                    'Molecule ' . $id . ' not found.'
                    ) ;
        }
        $form = $this->createForm(MoleculeType::class, $molecule) ;
        $form->handleRequest($request) ;        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager() ;
            $em->remove($molecule) ;
            $em->flush() ;
            $this->addFlash(
                    'notice', 
                    'Molecule ' . $id . ' deleted successfully.' 
                    ) ;
            return $this->redirectToRoute("chemicals_molecules_list") ;
            }       
        return $this->render("chemicals/molecules.delete.html.twig", [
            "form" => $form->createView() ,
        ]);
    }
    
    public function editAction(Request $request, $id)
    {
        $rep = $this->getDoctrine()->getRepository('ChemicalsBundle:Molecule') ;
        $molecule = $rep->findBy(['id' => $id]) ; 
        
        if (!empty($molecule))
        {
            $molecule = $molecule[0] ;
        }
        
        $form = $this->createForm(MoleculeType::class, $molecule) ;
        $form->handleRequest($request) ;
        
        if ($form->isSubmitted() && $form->isValid()) {
            // process métier
            $em = $this->getDoctrine()->getManager() ;
            $em->persist($molecule) ;
            $em->flush() ;
            $this->addFlash(
                'notice', 
                'Molecule ' . $molecule->getName() . ' modified successfully.' 
            );
            return $this->redirectToRoute('chemicals_molecules_list');
        }
        return $this->render("chemicals/molecules.edit.html.twig", [
            "form" => $form->createView() ,
        ]);
    }
}
