<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function adminIndexAction()
    {
        return $this->redirectToRoute("app_users_list");
    }
    
    public function listUsersAction()
    {
        $users = $this->get('fos_user.user_manager')->findUsers() ;
        return $this->render("users/list.html.twig", [
            'users' => $users ,
        ]) ;
    }
    
    public function setAdminAction($id)
    {
        $user = $this->get('fos_user.user_manager')->findUserby(['id' => $id]) ;
        $user->addRole('ROLE_ADMIN') ;
        $this->get('fos_user.user_manager')->updateUser($user) ;
        $this->addFlash('success', 'User Role switched to Admin') ;
        return $this->redirectToRoute("app_users_list") ;
    }

    public function unsetAdminAction($id)
    {
        $user = $this->get('fos_user.user_manager')->findUserby(['id' => $id]) ;
        $user->removeRole('ROLE_ADMIN') ;
        $this->get('fos_user.user_manager')->updateUser($user) ;
        $this->addFlash('success', 'User Role removed from Admin') ;
        return $this->redirectToRoute("app_users_list") ;
    }    

}
