<?php

namespace AppBundle\Controller ;

use Symfony\Bundle\FrameworkBundle\Controller\Controller ;
use ChemicalsBundle\Form\ElementType ;
use ChemicalsBundle\Entity\Element ;
use Symfony\Component\HttpFoundation\Request ;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/*
 * Element controller.
 * 
 * @author: Benoit Guibert
 */
class ElementController extends Controller 
{
    /*
     * add Element.
     * 
     */
    public function addAction(Request $request)
    {
        $element = new Element() ;
        
        $form = $this->createForm(ElementType::class, $element) ;
        $form->handleRequest($request) ;
        
        if ($form->isSubmitted() && $form->isValid()) {
            // process métier
            $em = $this->getDoctrine()->getManager() ;
            $em->persist($element) ;
            $em->flush() ;
            $this->addFlash(
                'notice', 
                'Element ' . $element->getName() . ' created successfully.' 
            );
            return $this->redirectToRoute('chemicals_elements_list');
        }
        
        return $this->render("chemicals/elements.add.html.twig", [
            "form" => $form->createView() ,
        ]);
    }


    /*
     * deletes Element.
     * 
     * @param type $id
     * 
     * @return type
     */
    public function deleteAction(Request $request, $id)
    {
        $rep = $this->getDoctrine()->getRepository('ChemicalsBundle:Element');
        $element = $rep->findBy(['id' => $id]) ; 
        if(!empty($element)) {
            $element = $element[0] ;
        } else {
            $this->addFlash(
                    'warning' ,
                    'Element ' . $id . ' not found.'
                    ) ;
        }
        $form = $this->createForm(ElementType::class, $element) ;
        $form->handleRequest($request) ;
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager() ;
            $em->remove($element) ;
            $em->flush() ;
            $this->addFlash(
                    'notice', 
                    'Element ' . $id . ' deleted successfully.' 
                    ) ;
            return $this->redirectToRoute("chemicals_elements_list") ;
            }       
        return $this->render("chemicals/elements.delete.html.twig", [
            "form" => $form->createView() ,
        ]);
    }
    
    public function editAction(Request $request, $id)
    {
        $rep = $this->getDoctrine()->getRepository('ChemicalsBundle:Element') ;
        $element = $rep->findBy(['id' => $id]) ; 
        
        if (!empty($element))
        {
            $element = $element[0] ;
        }
        
        $form = $this->createForm(ElementType::class, $element) ;
        $form->handleRequest($request) ;
        
        if ($form->isSubmitted() && $form->isValid()) {
            // process métier
            $em = $this->getDoctrine()->getManager() ;
            $em->persist($element) ;
            $em->flush() ;
            $this->addFlash(
                'notice', 
                'Element ' . $element->getName() . ' modified successfully.' 
            );
            return $this->redirectToRoute('chemicals_elements_list');
        }
        return $this->render("chemicals/element.edit.html.twig", [
            "form" => $form->createView() ,
        ]);
    }
}